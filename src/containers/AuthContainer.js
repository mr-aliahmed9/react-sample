import React, { Component } from 'react';
import { connect } from 'react-redux';

import { authenticate, logOut } from "../actions/user";

import Auth from "../components/auth/Auth";

import serialize from "form-serialize";

import { withRouter } from "react-router-dom";

function mapStatesToProps(state) {
  return {
    isAuthenticated: state.user.isAuthenticated,
    response: state.user.data
  };
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    onLogin: (e, modal) => {
      e.preventDefault();
      const form = e.target;
      const data = serialize(form, { hash: true });

      dispatch(authenticate(data, props, modal));
    },
    onLogOut: (e) => {
      e.preventDefault();
      dispatch(logOut(props));
    }
  }
};

const AuthContainer =  connect(mapStatesToProps, mapDispatchToProps)(Auth);

export default withRouter(AuthContainer);