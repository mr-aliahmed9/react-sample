import React, { Component } from 'react';
import { connect } from 'react-redux';

import { fetchUsers } from "../actions/users";

import UserList  from '../components/UserList';

class UserListContainer extends Component {
  componentWillMount() {
    this.props.fetchUsers();
  }

  render() {
    return(
        <UserList {...this.props} />
    )
  }
}

function mapStatesToProps(state) {
  return {
    users: state.users.data,
    errorOccurred: state.users.error,
    isAuthenticated: state.user.isAuthenticated
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchUsers: () => dispatch(fetchUsers())
  }
};

export default connect(mapStatesToProps, mapDispatchToProps)(UserListContainer);