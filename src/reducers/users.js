import {
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAILURE
} from '../actions/types.js';

import _merge  from 'lodash/merge';

const initialState = {
  error: false,
  data: []
};

export default function (state = initialState, action) {
  switch(action.type) {
    case FETCH_USERS_SUCCESS:
      // return _merge([...state], [...action.payload.data])
      // return {
      //   ...state,
      //   ...action.payload.data
      // }
      return {
        ...state,
        data: action.items
      };
    case FETCH_USERS_FAILURE:
      return {
        ...state,
        error: action.errorOccurred
      };
  }

  return state;
}