import '../node_modules/bootstrap/dist/css/bootstrap.css';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux'

import { authenticationSuccess } from './actions/user.js';

import {
  BrowserRouter as Router,
} from 'react-router-dom';

import App from './components/app';
import reducers from './reducers';

import Async from './middlewares/async';
import thunk from "redux-thunk";

const store = createStore(
    reducers,
    applyMiddleware(thunk)
);

// Check for user and update application state if required
const authenticatedUser = localStorage.getItem('user');
if (authenticatedUser) {
  store.dispatch(authenticationSuccess(JSON.parse(authenticatedUser)));
}

store.subscribe(() => {
  console.log(store.getState())
});

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>
  , document.querySelector('#root'));
