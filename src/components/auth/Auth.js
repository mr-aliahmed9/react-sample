import React, { Component } from 'react';
import { Button, NavItem, NavLink, Modal, ModalHeader, ModalBody, ModalFooter, Input, Label, Form, FormGroup } from 'reactstrap';
import { Redirect } from 'react-router';

// const Auth = ({ isAuthenticated, userData, onLogin, onLogOut }) => {
class Auth extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  loginForm() {
    return (
        <NavItem>
          <NavLink href="#" onClick={this.toggle} className="btn btn-outline-info btn-sm">Login</NavLink>
          <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
            <Form onSubmit={(e) => this.props.onLogin(e, this)}>
              <ModalHeader toggle={this.toggle}>Modal title</ModalHeader>
              <ModalBody>
                <FormGroup>
                  <Label for="email">Email</Label>
                  <Input type="text" name="email" id="email" placeholder="Enter Email" className="form-control" />
                </FormGroup>
                <FormGroup>
                  <Label for="password">Password</Label>
                  <Input type="password" name="password" id="password" placeholder="Enter Password" className="form-control" />
                </FormGroup>
              </ModalBody>
              <ModalFooter>
                <Button color="secondary" onClick={this.toggle}>Cancel</Button>{' '}
                <Button color="success">Login</Button>
              </ModalFooter>
            </Form>
          </Modal>
        </NavItem>
    )
  }

  render() {
    let { isAuthenticated, response, onLogOut } = this.props;
    return (
        <div>
          {
            isAuthenticated ? (
                <NavItem>
                  <NavLink href="#" onClick={e => onLogOut(e)}>{ response.username } Logout</NavLink>
                </NavItem>
            ) : this.loginForm()
          }
        </div>
    )
  }
};

export default Auth;