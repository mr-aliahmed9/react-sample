import React, { Component } from 'react';
import { Container } from 'reactstrap';

// Components
import Navigation from './layout/Navigation';
import FlashMessage from './layout/FlashMessage';

import UserListContainer from '../containers/UserListContainer';
import AboutUs from './AboutUs';
import ContactUs from './ContactUs';

// HOC
import RequireAuth from '../hoc/RequireAuth'

// Helpers
import { isEmpty } from '../helpers/AppHelper';

// React-Router
import {
  Route,
  Switch,
  withRouter
} from 'react-router-dom';

class App extends Component {
  constructor() {
    super();
    this.state = {
      flash: {}
    };
  };

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      if(typeof this.props.location.state === 'object') {
        this.setState({ flash: this.props.location.state.flash });
      } else {
        this.setState({ flash: {} });
      }
    }
  };

  renderFlashMessages = (flash) => {
    if(!isEmpty(flash)) {
      return(
          <FlashMessage type={flash.type} message={flash.message} />
      )
    }
  };

  render() {
    const flashMessages = this.renderFlashMessages(this.state.flash);
    return (
      <div>
        <div>
          <Navigation />
          <Container>
            { flashMessages }
            <Switch>
              <Route exact path="/" component={UserListContainer}></Route>
              <Route path="/about_us" component={AboutUs}></Route>
              {/*<Route path="/about_us" render={(props) => (<RequireAuth component={AboutUs} {...props} />)} />*/}
              {/*<Route path="/contact_us" component={RequireAuth(ContactUs)}></Route>*/}
              {/*<Route path="/contact_us" component={RequireAuth(AboutUs, 'Hey Dude, Get the Fuck out of here')} ></Route>*/}
              <Route path="/contact_us" render={(props) => (<RequireAuth component={ContactUs} message="Hey Dude, Get the Fuck out of here" {...props} />)} />
            </Switch>
          </Container>
        </div>
      </div>
    );
  }
}

export default withRouter(App);
