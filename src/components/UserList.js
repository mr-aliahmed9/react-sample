import React, { Component } from 'react';

import { Row, Col, Card, CardText,
  CardTitle, CardLink } from 'reactstrap';

const renderUser = (user) => {
  return (
      <Col key={user.id} md="3">
        <Card body className="user-card">
          <CardTitle style={{fontSize: '17px'}}>{user.name}</CardTitle>
          <small>Email: {user.email}</small>
          <CardText>
            <strong>Address: </strong>
            {user.address.street}
          </CardText>
          <CardLink className="btn btn-outline-info btn-sm" href={user.website || '#'}>Website</CardLink>
        </Card>
      </Col>
  );
};

class UserList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  render() {
    let { users, errorOccurred, isAuthenticated } = this.props;
    return (
        <div className="user-list">
          <Row>
            {
              !isAuthenticated ? (
                  <h2>Please Sign In</h2>
              ) : errorOccurred ? (
                  <h2>Items could not be fetched at the moment</h2>
              ) : users.map(renderUser)
            }
          </Row>
        </div>
    );
  };
}

// class UserList extends Component {
//   renderUser(user) {
//     return (
//       <Col key={user.id} md="3">
//         <Card body className="user-card">
//           <CardTitle style={{fontSize: '17px'}}>{user.name}</CardTitle>
//           <small>Email: {user.email}</small>
//           <CardText>
//             <strong>Address: </strong>
//             {user.address.street}
//           </CardText>
//           <CardLink className="btn btn-outline-info btn-sm" href={user.website || '#'}>Website</CardLink>
//         </Card>
//       </Col>
//     );
//   }
//
//   render() {
//     return (
//       <div className="user-list">
//         <Row>
//           { this.props.users.map(this.renderUser) }
//         </Row>
//       </div>
//     );
//   }
// }

export default UserList;