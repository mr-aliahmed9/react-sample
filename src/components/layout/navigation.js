import React, { Component } from 'react';
import { Collapse, Navbar, NavbarToggler, Nav, NavItem, NavLink, NavDropdown, DropdownItem, DropdownToggle, DropdownMenu } from 'reactstrap';
import { withRouter, NavLink as RouterNavLink } from 'react-router-dom';

import AuthContainer from '../../containers/AuthContainer';

class Navigation extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.dropDownToggle = this.dropDownToggle.bind(this);
    this.state = {
      isOpen: false,
      dropdownOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  dropDownToggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  render() {
    return(
      <div>
        <Navbar color="dark" dark expand="md">
          <NavbarToggler onClick={this.toggle} />
          <RouterNavLink exact to="/" activeClassName="active" className="navbar-brand">React Sample App</RouterNavLink>
          <Collapse isOpen={this.state.isOpen} navbar>

            <Nav className="mr-auto" navbar>
              <NavItem>
                <RouterNavLink exact to="/about_us" activeClassName="active" className="nav-link">About Us</RouterNavLink>
              </NavItem>
              <NavItem>
                <RouterNavLink exact to="/contact_us" activeClassName="active" className="nav-link">Contact Us</RouterNavLink>
              </NavItem>
              <NavDropdown isOpen={this.state.dropdownOpen} toggle={this.dropDownToggle}>
                <DropdownToggle nav caret>
                  Dropdown
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem header>Header</DropdownItem>
                  <DropdownItem disabled>Action</DropdownItem>
                  <DropdownItem>Another Action</DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem>Another Action</DropdownItem>
                </DropdownMenu>
              </NavDropdown>
            </Nav>

            <Nav className="ml-auto" navbar>
              <AuthContainer/>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    )
  }
}

export default withRouter(Navigation);
