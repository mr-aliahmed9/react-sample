export default function({ dispatch }) {
  return next => action => {
    // If action does not have payload
    // or, the payload does not have a .then property
    // we don't care about it, send it on
    if (!action.payload || !action.payload.then) {
      return next(action);
    }

    action.payload.then(response => {
      // Create a new action with the old type,
      // replace the promise with the response
      dispatch({ ...action, payload: response });
    }).catch(() => {
      alert('Hello')
    });
  }
}