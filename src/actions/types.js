export const FETCH_USERS_SUCCESS = 'fetch_users_success';
export const FETCH_USERS_FAILURE = 'fetch_users_failure';

export const USER_AUTH_SUCCESS = 'authentication_success';
export const USER_AUTH_FAILURE = 'authentication_failure';
export const USER_AUTH_LOGOUT_SUCCESS = 'logout_success';