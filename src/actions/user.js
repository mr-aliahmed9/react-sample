import axios from 'axios';

import {
  USER_AUTH_SUCCESS,
  USER_AUTH_FAILURE,
  USER_AUTH_LOGOUT_SUCCESS
} from './types';

export function authenticationSuccess(payload) {
  return {
    type: USER_AUTH_SUCCESS,
    payload
  };
}

export function authenticationFailure(errorMessage) {
  return {
    type: USER_AUTH_FAILURE,
    errorMessage: errorMessage
  };
}

export function logOutUser() {
  return {
    type: USER_AUTH_LOGOUT_SUCCESS
  };
}

export function authenticate(params, router, modal) {
  modal.toggle();
  return (dispatch) => {
    if (params.email === 'aliahmed@example.com' && params.password === 'aliahmed') {
      const response = {
        id: 1,
        username: 'Ali Ahmed',
        email: 'aliahmed@example.com'
      };
      localStorage.setItem('user', JSON.stringify(response));
      dispatch(authenticationSuccess(response));
      router.history.push(router.location.pathname, {
        flash: {
          type: 'success',
          message: 'You have signed in successfully.'
        }
      });
    } else {
      dispatch(authenticationFailure());
      router.history.push(router.location.pathname, {
        flash: {
          type: 'danger',
          message: 'Invalid email or password.'
        }
      });
    }
  }
}

export function logOut(router) {
  const response = {
    status: true
  };
  return (dispatch) => {
    localStorage.removeItem('user');
    dispatch(logOutUser(response));
    router.history.push(router.location.pathname, {
      flash: {
        type: 'success',
        message: 'You have signed out successfully.'
      }
    });
  }
}