import axios from 'axios';

import {
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAILURE
} from './types';

export function itemsFetchDataSuccess(items) {
  return {
    type: FETCH_USERS_SUCCESS,
    items
  };
}

export function itemsFetchDataFailure(bool = false) {
  return {
    type: FETCH_USERS_FAILURE,
    errorOccurred: bool
  };
}

export function fetchUsers() {
  const request = axios.get('http://jsonplaceholder.typicode.com/users');
  return (dispatch) => {
    request.then((response) => {
      dispatch(itemsFetchDataSuccess(response.data));
    }).catch(() => {
      dispatch(itemsFetchDataFailure(true));
    })
  }
}