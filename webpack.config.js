const path = require('path');

module.exports = {
  entry: [
    './src/index.js'
  ],
  output: {
    path: __dirname,
    publicPath: '/',
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        exclude: [
          path.resolve(__dirname, "/node_modules/")
        ],
        loader: 'babel-loader',
        options: {
          plugins: ['lodash'],
          presets: ['react', 'es2015', 'stage-1']
        }
      }, {
        test: /\.css|sass|scss$/,
        use: [
          {
            loader: 'style-loader',
            options: {
              insertAt: 'top',
            }
          }, {
            loader: "css-loader"
          }
        ]
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  devServer: {
    historyApiFallback: true,
    contentBase: './',
    inline: true,
    port: 8008
  }
};
